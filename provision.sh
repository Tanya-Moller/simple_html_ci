#!/bin/bash

sudo yum -y install httpd
if rpm -qa | grep "^httpd-[0-9]" >/dev/null 2>&1
then
  sudo systemctl start httpd
else
  exit 1
fi
sudo sh -c "cat >/var/www/html/index.html <<_END_
<h1>
 Testing this server 2🔥
</h1>
<h3>refresh for a new picture</h3>
<img src="https://source.unsplash.com/random" alt="">
_END_"
